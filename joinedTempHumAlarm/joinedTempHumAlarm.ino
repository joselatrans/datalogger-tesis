#include <SD.h>
#include <RTCZero.h>
#include "DHT.h"

#define DHTPIN1 16 // # Camara 1
#define DHTPIN2 17 // # Camara 2
#define DHTPIN3 18 // # Camara 3
#define DHTPIN4 19 // # Invernadero
#define DHTTYPE DHT22   // Which type of DHT sensor you're using: 

// initialize the sensor:
DHT dht1(DHTPIN1, DHTTYPE);
DHT dht2(DHTPIN2, DHTTYPE);
DHT dht3(DHTPIN3, DHTTYPE);
DHT dht4(DHTPIN4, DHTTYPE);

const int chipSelect = 4;         // SPI chip select for SD card
const int cardDetect = 7;         // pin that detects whether the card is there

const int writeLed = 8;           // LED indicator for writing to card
const int errorLed = 13;          // LED indicator for error

//-------------------
//-------EDITAR------
//-------------------
//Funciones del reloj
//-------------------
RTCZero rtc;
const byte days = 27;
const byte months = 06;
const byte years = 19;
//-------------------
//-------------------
//-------------------

char fileName[] = "datalog.csv";  // filename to save on SD card

void setup() {
  Serial.begin(9600);   // initialize serial communication

  // initialize LED and cardDetect pins:
  pinMode(writeLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  pinMode(cardDetect, INPUT_PULLUP);

  // startSDCard() blocks everything until the card is present
  // and writable:
  if (startSDCard() == true) {
    Serial.println("card initialized.");
    delay(100);
    // open the log file:
    File logFile = SD.open(fileName, FILE_WRITE);
    // write header columns to file:
    if (logFile) {
      logFile.println("Fecha, Hora, T_CAM1, T_CAM2, T_CAM3, T_CAM4, H_CAM1, H_CAM2, H_CAM3, H_CAM4");
      logFile.close();
    }
  }
  
  // initialize the sensor:
  dht1.begin();
  dht2.begin();
  dht3.begin();
  dht4.begin();

  rtc.begin();                    // start the real-time clock
  setCompileTime();               // set the clock from the time you compiled
  printTime();                    // print the time
  
  rtc.setAlarmTime(0, 0, 59);      // set an alarm
  rtc.enableAlarm(rtc.MATCH_SS);  // enable the alarm, match seconds only
  rtc.attachInterrupt(logData);   // when the alarm happens, run logData
}

void loop() {
  // the loop isn't doing anything in this sketch
}

// this is called by the RTC alarm once a minute
void logData() {
   File logFile =SD.open(fileName, FILE_WRITE);
   if (logFile) {                      // if you can write to the log file,
      digitalWrite(writeLed, HIGH);    // turn on the write LED
      printTime();
      printAmbient();
      logFile.close();
    }
    digitalWrite(writeLed, LOW);
} //  CLOSES logData

/*
  This function sets the realtime clock using the time
  that the sketch was compiled on your PC. It sets the date
  from the global constants at the top of the sketch
*/
void setCompileTime() {
  // get the time from the compiler:
  String timeString = __TIME__;
  byte timeArray[3];    //  array for the elements of the time
  int t = 0;            // array counter

  // iterate over the string, reading the first number
  // then removing it from the string, until
  // the string is gone:
  while (timeString.length() > 0) {
    // read beginning of string, convert to integer:
    byte nextNumber = timeString.toInt();
    // save number in the array:
    timeArray[t] = nextNumber;
    // remove the first three chars of the array:
    timeString.remove(0, 3);
    // increment the counter:
    t++;
  }
  // set the time using the array and
  // the date using the constants from above:
  rtc.setTime(timeArray[0], timeArray[1], timeArray[2]);
  rtc.setDate(days, months, years);
} // CLOSES setCompileTime

/*
   Prints the time as DD/MM/YY,HH:M:SS
*/
void printTime() {
  File logFile =SD.open(fileName, FILE_WRITE);
  // print to the log file:
      logFile.print(rtc.getDay());
      logFile.print("/");
      logFile.print(rtc.getMonth());
      logFile.print("/");
      logFile.print(rtc.getYear());
      logFile.print(",");
      logFile.print(rtc.getHours());
      logFile.print(":");
      logFile.print(rtc.getMinutes());
      logFile.print(":");
      logFile.print(rtc.getSeconds());
      logFile.print(",");
      logFile.close();

   // for debugging only:
      print2Digits(rtc.getDay());
      Serial.print("/");
      print2Digits(rtc.getMonth());
      Serial.print("/");
      print2Digits(rtc.getYear());
      Serial.print(",");
      print2Digits(rtc.getHours());
      Serial.print(":");
      print2Digits(rtc.getMinutes());
      Serial.print(":");
      print2Digits(rtc.getSeconds());
      Serial.print(",");
} // CLOSES printTime

// prints single-digit numbers as 2-digit
void print2Digits(byte number) {
  if (number <= 9) {
    Serial.print("0");
  }
  Serial.print(number);
} // CLOSES print2Digits

void printAmbient(){
  File logFile =SD.open(fileName, FILE_WRITE);
  // read sensor:
    float humidity1 = dht1.readHumidity();  //DATOS DEL SENSOR 1 (16)
    float temperature1 = dht1.readTemperature();
    float humidity2 = dht2.readHumidity();  //DATOS DEL SENSOR 2 (17)
    float temperature2 = dht2.readTemperature();
    float humidity3 = dht3.readHumidity();  //DATOS DEL SENSOR 3 (18)
    float temperature3 = dht3.readTemperature();
    float humidity4 = dht4.readHumidity();  // DATOS DEL SENSOR 4 (19)
    float temperature4 = dht4.readTemperature();

   // print to the log file:
   //----------TEMPERATURA--------
      logFile.print(temperature1);
      logFile.print(",");
      logFile.print(temperature2);
      logFile.print(",");
      logFile.print(temperature3);
      logFile.print(",");
      logFile.print(temperature4);
      logFile.print(",");
    //----------HUMEDAD--------
      logFile.print(humidity1);
      logFile.print(",");
      logFile.print(humidity2);
      logFile.print(",");
      logFile.print(humidity3);
      logFile.print(",");
      logFile.println(humidity4);
      logFile.close();    // close the file

   // for debugging only:
      Serial.print(temperature1);
      Serial.print(",");
      Serial.print(temperature2);
      Serial.print(",");
      Serial.print(temperature3);
      Serial.print(",");
      Serial.print(temperature4);
      Serial.print(",");
      Serial.print(humidity1);
      Serial.print(",");
      Serial.print(humidity2);
      Serial.print(",");
      Serial.print(humidity3);
      Serial.print(",");
      Serial.println(humidity4);
} // CLOSES printAmbient

boolean startSDCard() {
  // Wait until the card is inserted:
  while (digitalRead(cardDetect) == LOW) {
    Serial.println("Waiting for card...");
    digitalWrite(errorLed, HIGH);
    delay(750);
  }

  // wait until the card initialized successfully:
  while (!SD.begin(chipSelect)) {
    digitalWrite(errorLed, HIGH);   // turn on error LED
    Serial.println("Card failed");
    delay(750);
  }
  return true;
} // CLOSES startSDCard
