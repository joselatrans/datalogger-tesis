#include <DHT.h>
#include <RTClib.h>

#define DHTPIN1 17 //PIN DEL SENSOR
#define DHTPIN2 18
#define DHTPIN3 19

#define DHTTYPE DHT22

DHT dht1(DHTPIN1, DHTTYPE);
DHT dht2(DHTPIN2, DHTTYPE);
DHT dht3(DHTPIN3, DHTTYPE);

RTC_DS3231 rtc;
char fechayhora[20];

int in = 8; // RESISTENCIA(S)

void inicioRTC(void){
  rtc.begin();
//Configura la hora y fecha automaticamente
  rtc.adjust(DateTime(F(__DATE__),F(__TIME__)));
}

void setup() {
  Serial.begin(9600);

  inicioRTC();

  dht1.begin();
  dht2.begin();
  dht3.begin();
  
  pinMode(in, OUTPUT);
  digitalWrite(in, HIGH);
}//Cierra setup

void loop() {
  delay(2000);

  DateTime now = rtc.now(); 
  int  hora = now.hour();

  dht1.begin();
  dht2.begin();
  dht3.begin();
  
  float h1 = dht1.readHumidity();  //DATOS DEL SENSOR 1
  float t1 = dht1.readTemperature();
  float h2 = dht2.readHumidity();  //DATOS DEL SENSOR 2
  float t2 = dht2.readTemperature();
  float h3 = dht3.readHumidity();  //DATOS DEL SENSOR 3
  float t3 = dht3.readTemperature();

  if(hora > 19 && hora < 5){
    if(t1 or t2 or t3 <= 30){
      digitalWrite(in, LOW);
      delay(90000);
      digitalWrite(in, HIGH);
      delay(30000);
      digitalWrite(in, LOW);
      delay(2000);
    } else {
      digitalWrite(in, HIGH);
      delay(2000);
    }
	} else {
    	if(t1 or t2 or t3 <= 25){
      	digitalWrite(in, LOW);
      	delay(90000);
      	digitalWrite(in, HIGH);
      	delay(30000);
      	digitalWrite(in, LOW);
      	delay(2000);
    	} else {
 				}
		}
} // CLOSES loop
