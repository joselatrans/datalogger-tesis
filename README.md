# Datalogger tesis

Jose Lopez Rodriguez

1. datlogBattery:

Este es un datalogger que funciona con baterias por lo cual requiere ser energeticamente más eficiente. Usa la libreria LowPower para dormirse profundamente por 8s cada cierto tiempo. 

2. tempDatalogger:

Este es un datalogger que depende de una conexion directa a corriente o a una bateria grande.
Tiene un luxometro que el de bateria no tiene.
Genera archivo log.txt

3. find_ds18b20:

Este codigo permite conocer las coordenadas de cada sensor de temperatura.

4. rhDatalogger:

Este datalogger funciona conectado a la corriente o a una bateria grande. Lee la hora y fecha automaticamente de la compu. Actualmente usa sensores DHT22, sin embargo se espera cambiar de sensores los cuales se conectarian por el protocolo I2C, probablemente SHT85. 

## Ambos

Ambos dataloggers tienen capacidad de iniciar las mediciones a una hora determinada.

## Falta:

1. Que se lea el documento antes de escribir y si ya existe y tiene informacion que se haga uno nuevo y se comienze a escribir sobre el nuevo
2. Que cada archivo tenga un nombre base y un numero consecutivo según los demás existentes
3. Que la toma de datos se inicie y realize a través de alarmas y no del botón ni por delays.
